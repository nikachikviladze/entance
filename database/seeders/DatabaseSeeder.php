<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(20)->create();
        \App\Models\Facility::factory(20)->create();

        $fac = \App\Models\Facility::all();


        \App\Models\User::all()->each(function ($user) use ($fac) { 
            $ids = $fac->random(rand(1, 3))->pluck('id')->toArray();
            $user->facilities()->attach($ids); 

            foreach ($ids as $value) {
                $card = new \App\Models\Card();
                $card->user_id = $user->id;
                $card->facility_id = $value;
                $card->uuid = Str::uuid()->toString();
                $card->save();
            }

        });
        
    }
}
