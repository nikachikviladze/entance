<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Entrance;
use App\Models\Facility;

class PagesController extends Controller
{
    public function index()
    {
        $facility = Facility::first();

        return view('welcome', compact('facility'));
    }
    public function reception(Request $request)
    {        
        try
        {
            $facility = Facility::where('uuid', $request->facility_uuid)->firstOrFail();

            $card = $facility->cards->where('uuid', $request->card_uuid)->first();
            if(!$card){
                return response()->json(['message'=> "error"], 404);
            }

            $entrance = $card->entrance->where('created_at', '>=', date('Y-m-d').' 00:00:00')->first();
            
            if($entrance==null){

                $new_entrance = new Entrance();
                $new_entrance->card_id = $card->id;
                $new_entrance->save();

                return response()->json(['message'=> "{$card->user->name} {$card->user->last_name} entered facility: {$card->facility->name} successfully"], 202);

            }

            return response()->json(['message'=> "{$card->user->name} {$card->user->last_name} have already been in facility: {$card->facility->name} this day. Date : {$entrance->created_at}"], 202);
        }
        catch(ModelNotFoundException $e)
        {

            return response()->json(['message'=> "error"], 404);
        }
        
    }
}
