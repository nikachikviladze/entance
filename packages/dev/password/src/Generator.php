<?php

namespace DevNch\Password;

class Generator
{
    public function generator()
    {
        $alphabet =  "abcdefghijklmnopqrstuvwxyz";
        $numbers = "2345";
        $symbols = "!#$%&(){}[]=";

        function getSymbol($string){
            return $string[rand(0, strlen($string)-1)].$string[rand(0, strlen($string)-1)];
        }

        $uppercase = strtoupper(getSymbol($alphabet));
        $lowercase = strtolower(getSymbol($alphabet));
        $numbers = getSymbol($numbers);
        $symbols = getSymbol($symbols);

        $low = $uppercase.$lowercase.$uppercase;
        $medium = $uppercase.$lowercase.$numbers;
        $high = $uppercase.$lowercase.$numbers.$symbols;


        function random_string($first, $second, $third){

            $array = array($first, $second, $third);
            $ct = rand(0, count($array) - 1 );
            $Strength = $ct+1;
            return str_shuffle($array[$ct]).'    || Strength '. $Strength;
        }





        return random_string($low,$medium,$high) ;
    }
}
