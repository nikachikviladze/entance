<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    </head>
    <body>
        <div id="app" class="container">
            <h3 class="text-center mt-2">1. System for storing entrances to the sports facilities</h3>
            <div class="row justify-content-center">
                <div class="col-md-8 ">
                    <p>@{{this.message}}</p>
                    <form @submit.prevent="reception" action="reception" method="POST">
                        <div class="form-group mb-2">
                            <label for="exampleInputPassword1">Facility UUID</label>
                            <input v-model="facility_uuid" class="form-control" name="facility_uuid" placeholder="Facility uuid">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Card UUID</label>
                            <input v-model="card_uuid" class="form-control" name="card_uuid" id="exampleInputPassword1" placeholder="Card uuid">
                        </div>
                        <button type="submit" class="mt-2 btn btn-primary">Submit</button>
                    </form>   
                </div>                
            </div>
            @if($facility)
            <div class="mt-2 row justify-content-center">
                <p class="text-center ">Example</p>
                <div class="col-md-4">
                    <p>Facility uuid</p>
                    <hr>
                    <p>
                        {{$facility->uuid}}
                    </p>
                </div>
                <div class="col-md-4">
                    <p>Cards uuid:</p>
                    <hr>
                    <ul>
                        @foreach($facility->cards as $card)
                        <li>{{$card->uuid}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>  
            @endif   
            
            
            <h3 class="text-center mt-2">2. Password generator</h3>
            <div class="row justify-content-center">
                <div class="col-md-8 ">
                    <h4>@{{this.password}}</h4>
                    <form @submit.prevent="password_generator" action="password_generator" method="GET">
                        <button type="submit" class="mt-2 btn btn-primary">Generate password</button>
                    </form>   
                </div>                
            </div>

        </div>

        <script src="js/vue.js"></script>
        <script src="/js/axios.min.js"></script>
        <script>
            const app = new Vue({
                el: '#app',

                data() {
                    return{  
                        card_uuid:null,
                        facility_uuid:null,
                        message:null,
                        password:null
                    }
                },
                methods:{

                    reception(){
                        this.message=null

                        axios.post(`/reception`, {facility_uuid:this.facility_uuid, card_uuid: this.card_uuid}).then(resp=>{
                                this.message = resp.data.message

                            }) .catch(function (error) {
                                alert('error')

                            })

                    },
                    password_generator(){
                        this.password=null

                        axios.get(`/password_generator`).then(resp=>{
                                this.password = resp.data
                                // console.log(resp.data)

                            }) .catch(function (error) {
                                alert('error')

                            })

                    }
                }
            });

        </script>



    </body>
</html>

