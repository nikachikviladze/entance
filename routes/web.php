<?php

use Illuminate\Support\Facades\Route;

use DevNch\Password\Generator;

Route::get('password_generator', function() {
    $password = new Generator();
    return $password->generator();
});

Route::get('/', [App\Http\Controllers\PagesController::class, 'index']);
Route::get('reception', function(){
    abort(404);
});
Route::post('reception', [App\Http\Controllers\PagesController::class, 'reception']);
